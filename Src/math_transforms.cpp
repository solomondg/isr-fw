//
// Created by solomon on 4/20/20.
//

#include "math_transforms.hpp"

using namespace math_util;

namespace math_transforms::svpwm {

  bool svpwm_anglemag(f32 angle, f32 magnitude, vector3 &phase_mag) {
    magnitude = clamp(0.f, magnitude, 1.f);
    f32 a = cosf(angle) * magnitude;
    f32 b = sinf(angle) * magnitude;
    return svpwm_ab(a, b, phase_mag);
  }

  bool svpwm_ab(f32 a, f32 b, vector3 &phase_mag) {
    a = clamp(-1.f, a, 1.f);
    b = clamp(-1.f, b, 1.f);

    vector3 _phase_mag{0.f, 0.f, 0.f};

    _phase_mag.a = 0.f;
    _phase_mag.b = 0.f;
    _phase_mag.c = 0.f;

    enum { s1, s2, s3, s4, s5, s6 } sector;

    if (b >= 0.f) {
      if (a >= 0.f) { // Quadrant I
        if (b * constants::invsqrt3 > a)
          sector = s2;
        else
          sector = s1;
      } else { // Quadrant II
        if (-b * constants::invsqrt3 > a)
          sector = s3;
        else
          sector = s2;
      }
    } else {
      if (a >= 0.f) { // Quadrant IV
        if (-b * constants::invsqrt3 > a)
          sector = s5;
        else
          sector = s6;
      } else { // Quadrant III
        if (b * constants::invsqrt3 > a)
          sector = s4;
        else
          sector = s5;
      }
    }

    f32 t1, t2, t3, t4, t5, t6;
    //f32 t1, t2;

    switch (sector) {

    case s1:
      // Vector on times
      t1 = a - b * constants::invsqrt3;
      t2 = b * constants::invsqrt3_x2;

      // PWM timings
      _phase_mag.a = (1.f - t1 - t2) * .5f;
      _phase_mag.b = _phase_mag.a + t1;
      _phase_mag.c = _phase_mag.b + t2;
      break;

    case s2:

      t2 = a + b * constants::invsqrt3;
      t3 = -a + b * constants::invsqrt3;

      _phase_mag.b = (1.f - t2 - t3) * .5f;
      _phase_mag.a = _phase_mag.b + t3;
      _phase_mag.c = _phase_mag.a + t2;
      break;

    case s3:

      t3 = b * constants::invsqrt3_x2;
      t4 = -a - b * constants::invsqrt3;

      _phase_mag.b = (1.f - t3 - t4) * .5f;
      _phase_mag.c = _phase_mag.b + t3;
      _phase_mag.a = _phase_mag.c + t4;

      break;

    case s4:

      t4 = -a + b * constants::invsqrt3;
      t5 = -b * constants::invsqrt3_x2;

      _phase_mag.c = (1.f - t4 - t5) * .5f;
      _phase_mag.b = _phase_mag.c + t5;
      _phase_mag.a = _phase_mag.b + t4;

      break;

    case s5:

      t5 = -a - b * constants::invsqrt3;
      t6 = a - b * constants::invsqrt3;

      _phase_mag.c = (1.f - t5 - t6) * .5f;
      _phase_mag.a = _phase_mag.c + t5;
      _phase_mag.b = _phase_mag.a + t6;

      break;
    case s6:

      t6 = -b * constants::invsqrt3_x2;
      t1 = a * b * constants::invsqrt3;

      _phase_mag.a = (1.f - t6 - t1) * .5f;
      _phase_mag.c = _phase_mag.a + t1;
      _phase_mag.b = _phase_mag.c + t6;

      break;
    }

    i32 result_valid =
        _phase_mag.a >= 0.f && _phase_mag.a <= 1.f &&
            phase_mag.b >= 0.f && phase_mag.b <= 1.f &&
            phase_mag.c >= 0.f && phase_mag.c <= 1.f;

    return (result_valid) ? true : false;
  }
}

