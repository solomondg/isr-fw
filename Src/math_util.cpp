//
// Created by solomon on 4/20/20.
//

#include <math.h>
#include <cfloat>
#include "math_util.hpp"

// based on https://math.stackexchange.com/a/1105038/81278
f32 math_util::fast_atan2(f32 y, f32 x) {
  // a := min (|x|, |y|) / max (|x|, |y|)
  f32 abs_y = fabsf(y);
  f32 abs_x = fabsf(x);
  // inject FLT_MIN in denominator to avoid division by zero
  f32 a = min(abs_x, abs_y) / (max(abs_x, abs_y) + FLT_MIN);
  // s := a * a
  f32 s = a * a;
  // r := ((-0.0464964749 * s + 0.15931422) * s - 0.327622764) * s * a + a
  f32 r = ((-0.0464964749f * s + 0.15931422f) * s - 0.327622764f) * s * a + a;
  // if |y| > |x| then r := 1.57079637 - r
  if (abs_y > abs_x)
    r = 1.57079637f - r;
  // if x < 0 then r := 3.14159274 - r
  if (x < 0.0f)
    r = 3.14159274f - r;
  // if y < 0 then r := -r
  if (y < 0.0f)
    r = -r;

  return r;
}