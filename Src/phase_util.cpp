#include <mappings.hpp>
#include <cmath>
#include "phase_util.hpp"

namespace phase_util {

  void set_phase(const phase ph, mode md, f32 mag) {
    set_phase(ph, md, (u16) roundf(PHASE_ARR * mag));
  }

  void set_phase(const phase ph, mode md, u16 mag) {

    bool en = false;
    u16 mod_amnt = 0;
    u32 channel;

    switch (ph) {
      case phase::A:
        channel = map::PWM::inverter::phase_A::channel;
        break;
      case phase::B:
        channel = map::PWM::inverter::phase_B::channel;
        break;
      case phase::C:
        channel = map::PWM::inverter::phase_C::channel;
        break;
    }

    switch (md) {
      case mode::MOD: // Modulate as half bridge
        en = true; // Turn on both PWM outputs
        mod_amnt = mag; // Modulate as much as the input magnitude
        break;
      case mode::GND: // Low side on, SW to GND
        en = true; // Turn off both PWM inputs, forcing to gnd
        mod_amnt = 0; // Force on low side FET
        break;
      case mode::HIZ: // Pull gates to source, float SW node
        en = false; // Disable high, pull to gnd (HS FET off)
        mod_amnt = 0; // Force low to VCC (LS FET off)
        break;
    }

    if (en) {
      __HAL_TIM_SET_COMPARE(map::PWM::inverter::timer, channel, mod_amnt);
      HAL_TIM_PWM_Start(map::PWM::inverter::timer, channel);
      HAL_TIMEx_PWMN_Start(map::PWM::inverter::timer, channel);
    } else {
      HAL_TIM_PWM_Stop(map::PWM::inverter::timer, channel);
      HAL_TIMEx_PWMN_Stop(map::PWM::inverter::timer, channel);
      __HAL_TIM_SET_COMPARE(map::PWM::inverter::timer, channel, mod_amnt);

    }

  }

  void hall_commute(u8 hall_code, u16 power, bool direction) {
    /*
    PHASE 1: A: 1, B: 0, C: 0
    PHASE 2: A: 1, B: 1, C: 0
    PHASE 3: A: 0, B: 1, C: 0
    PHASE 4: A: 0, B: 1, C: 1
    PHASE 5: A: 0, B: 0, C: 1
    PHASE 6: A: 1, B: 0, C: 1
     */


    switch (hall_code) {
      case 0b100:
        hall_code = direction ? 0b110 : 0b101;
        break;
      case 0b110:
        hall_code = direction ? 0b010 : 0b100;
        break;
      case 0b010:
        hall_code = direction ? 0b011 : 0b110;
        break;
      case 0b011:
        hall_code = direction ? 0b001 : 0b010;
        break;
      case 0b001:
        hall_code = direction ? 0b101 : 0b011;
        break;
      case 0b101:
        hall_code = direction ? 0b100 : 0b001;
        break;
    }

    /*
    if (direction) {
      hall_code += 1;
      if (hall_code == 0b111)
        hall_code = 0b001;
    } else {
      hall_code -= 1;
      if (hall_code == 0b000)
        hall_code = 0b110;
    }
     */

    switch (hall_code) {
      case 0b100:
        set_phase(phase::A, mode::MOD, power);
        set_phase(phase::B, mode::HIZ);
        set_phase(phase::C, mode::GND);
        break;
      case 0b110:
        set_phase(phase::A, mode::MOD, power);
        set_phase(phase::B, mode::GND);
        set_phase(phase::C, mode::HIZ);
        break;
      case 0b010:
        set_phase(phase::A, mode::HIZ);
        set_phase(phase::B, mode::GND);
        set_phase(phase::C, mode::MOD, power);
        break;
      case 0b011:
        set_phase(phase::A, mode::GND);
        set_phase(phase::B, mode::HIZ);
        set_phase(phase::C, mode::MOD, power);
        break;
      case 0b001:
        set_phase(phase::A, mode::GND);
        set_phase(phase::B, mode::MOD, power);
        set_phase(phase::C, mode::HIZ);
        break;
      case 0b101:
        set_phase(phase::A, mode::HIZ);
        set_phase(phase::B, mode::MOD, power);
        set_phase(phase::C, mode::GND);
        break;
    }

  }
}
