#include <drv8323.hpp>
#include "phase_util.hpp"
#include <cstdio>
#include "main.h"
#include "adc.h"
#include "can.h"
#include "i2c.h"
#include "spi.h"
#include "tim.h"
#include "usart.h"
#include "usb.h"
#include "gpio.h"
#include "a4928.hpp"
#include "math_transforms.hpp"
#include "math_util.hpp"

#include "mappings.hpp"

extern "C" void initialise_monitor_handles(void);

u32 get_phase_delay_for_rpm(u16 rpm) {
  // 60 rpm -> 1 rev/second
  // = 21 phase transitions/second
  float trans_per_s = 21.f * (f32) rpm / 60.f;
  float ms_per_trans = 1000.f / trans_per_s;
  float us_per_trans = 1000.f * ms_per_trans;
  return (u32) us_per_trans;
}

uint32_t __attribute__((hot)) micros() {
  uint32_t ms, cycle_cnt;
  do {
    ms = HAL_GetTick();
    cycle_cnt = __HAL_TIM_GET_COUNTER(&htim6);
  } while (ms != HAL_GetTick());

  return (ms * 1000) + cycle_cnt;
}
void __attribute__((hot)) delay_us(uint32_t us) {
  uint32_t start = micros();
  while (micros() - start < (uint32_t) us) {
    __ASM("nop");
  }
}

static inline void delay(u16 us) { HAL_Delay(us); }

extern "C" int main() {

  HAL_Init();

  SystemClock_Config();

  MX_GPIO_Init();
  MX_ADC1_Init();
  MX_ADC2_Init();
  MX_ADC4_Init();
  MX_CAN_Init();
  MX_I2C1_Init();
  MX_I2C3_Init();
  MX_SPI1_Init();
  MX_SPI2_Init();
  MX_SPI3_Init();
  MX_TIM1_Init();
  MX_TIM2_Init();
  MX_TIM3_Init();
  MX_TIM4_Init();
  MX_TIM6_Init();
  MX_TIM8_Init();
  MX_TIM15_Init();
  MX_TIM16_Init();
  MX_TIM17_Init();
  MX_USART1_UART_Init();
  MX_USB_PCD_Init();

  HAL_TIM_Base_Start(&htim6);
  //initialise_monitor_handles();

  a4928::set_enable(false);
  a4928::enable_reset(true);
  drv::set_enable(false);

  delay(10);
  drv::set_enable(false);
  delay(10);

  // Both started -> Synchronous rectification
  // Both stopped -> Hi-Z

  phase_util::set_phase(phase_util::phase::A, phase_util::mode::GND);
  phase_util::set_phase(phase_util::phase::B, phase_util::mode::GND);
  phase_util::set_phase(phase_util::phase::C, phase_util::mode::GND);

  delay(10);
  drv::set_enable(true);
  delay(10);

  drv::calibrate();

  delay(10);

  u16 del= 6;
  u16 pow = 80;
  //phase_util::hall_commute(0b100,250,true);
  //while(1);

  while (1) {
    phase_util::hall_commute(0b100,pow,true);
    delay(del);
    phase_util::hall_commute(0b110,pow,true);
    delay(del);
    phase_util::hall_commute(0b010,pow,true);
    delay(del);
    phase_util::hall_commute(0b011,pow,true);
    delay(del);
    phase_util::hall_commute(0b001,pow,true);
    delay(del);
    phase_util::hall_commute(0b101,pow,true);
    delay(del);

  }

}
