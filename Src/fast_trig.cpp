#include <stm32f3xx_hal.h>  // Sets up the correct chip specifc defines required by arm_math
#define ARM_MATH_CM4 // TODO: might change in future board versions

#include "fast_trig.hpp"

/*
f32 fast_sin(
    f32 x) {
  f32 sinVal, fract, in;
  uint16_t index;
  f32 a, b;
  int32_t n;
  f32 findex;

  in = x * 0.159154943092f;

  n = (int32_t) in;

  if (x < 0.0f) {
    n--;
  }

  in = in - (f32) n;

  findex = (f32) FAST_MATH_TABLE_SIZE * in;
  index = (uint16_t) findex;

  if (index >= FAST_MATH_TABLE_SIZE) {
    index = 0;
    findex -= (f32) FAST_MATH_TABLE_SIZE;
  }

  fract = findex - (f32) index;

  a = sinTable_f32[index];
  b = sinTable_f32[index + 1];

  sinVal = (1.0f - fract) * a + fract * b;

  return (sinVal);
}

f32 fast_cos(
    f32 x) {
  f32 cosVal, fract, in;
  uint16_t index;
  f32 a, b;
  int32_t n;
  f32 findex;

  in = x * 0.159154943092f + 0.25f;

  n = (int32_t) in;

  if (in < 0.0f) {
    n--;
  }

  in = in - (f32) n;

  findex = (f32) FAST_MATH_TABLE_SIZE * in;
  index = (uint16_t) findex;

  if (index >= FAST_MATH_TABLE_SIZE) {
    index = 0;
    findex -= (f32) FAST_MATH_TABLE_SIZE;
  }

  fract = findex - (f32) index;

  a = sinTable_f32[index];
  b = sinTable_f32[index + 1];

  cosVal = (1.0f - fract) * a + fract * b;

  return (cosVal);
}

 */