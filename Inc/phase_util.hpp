#pragma once
#include "main.h"

namespace phase_util {

  enum class phase { A, B, C };
  enum class mode { MOD, GND, HIZ };

  constexpr u16 PHASE_ARR = 2047;

  void set_phase(phase ph, mode md, u16 mag = 0);
  void set_phase(phase ph, mode md, f32 mag);
  //void set_phases(u8 hall_code, u16 power, bool direction);

  void hall_commute(u8 hall_code, u16 power, bool direction);

}