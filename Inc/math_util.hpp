#pragma once

#include "main.h"

namespace math_util {

  struct vector3 {
    f32 a;
    f32 b;
    f32 c;
  };

  static inline constexpr i32 mod(i32 dividend, i32 divisor) {
    i32 r = dividend % divisor;
    return (r < 0) ? (r + divisor) : r;
  }

  template<class T> static inline constexpr T min(T a, T b) { return (a < b) ? a : b; }
  template<class T> static inline constexpr T max(T a, T b) { return (a > b) ? a : b; }
  template<class T> static inline constexpr T clamp(T min_val, T value, T max_val) { return min<T>(max_val, max<T>(value, min_val)); }

  f32 fast_atan2(f32 y, f32 x);

  namespace constants {
    constexpr f32 invsqrt3 = 0.5773502691896258;
    constexpr f32 invsqrt2 = 0.7071067811865475;
    constexpr f32 invsqrt3_x2 = invsqrt3 * 2;
    constexpr f32 invsqrt2_x2 = invsqrt2 * 2;
    constexpr f32 sqrt2 = 1.4142135623730951;
    constexpr f32 sqrt3 = 1.7320508075688772;
    constexpr f32 sqrt2_onehalf = sqrt2 / 2;
    constexpr f32 sqrt3_onehalf = sqrt3 / 2;
    constexpr f32 pi = M_PI;

  }

}

