#pragma once

#include <cmath>
#include "math_util.hpp"

namespace math_transforms {
  namespace svpwm {

/*
 * angle: radians, 0-2pi
 * magnitude: 0-1
 * output: three phase magnitudes
 */
    bool svpwm_anglemag(f32 angle, f32 magnitude, math_util::vector3 &phase_mag);

/*
 * a: -1 to 1
 * b: -1 to 1
 * output: three phase magnitudes
 */
    bool svpwm_ab(f32 &a, f32 &b, math_util::vector3 &phase_mag);
  }
}